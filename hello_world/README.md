`helloWorld.js` startet einen GraphQL-Server auf Port __4001__ (für alle Fälle, damit er später nicht mit dem Wer-ist-es-Server auf 4000 kollidiert). Die Abfrage kann auf drei Arten demonstriert werden:

---

### 1. cURL

Für Terminal-Fans. cURL gibt es gratis zu __Linux__ dazu oder für Windows-User in der __GitBash__ oder dem großartigen Konsolenemulator __cmder__. So sieht der Aufruf aus:


```sh
curl -X POST -H "Content-Type: application/json" --data '{ "query": "{ hello }" }' http://localhost:4001 -s
```

* `-X POST` bestimmt die Methode.
* `-H "Content-Type: application/json"` setzt den Header.
* `--data '{ "query": "{ hello }" }'` definiert den Body (wie 1 body builder).
* `http://localhost:4001` ist der Wohnort des Servers.
* `-s` ist der Silent-Modus. Wir erhalten nur den Body der Response, der Rest interessiert uns hier nicht.

Die Antwort:

```sh
{"data":{"hello":"Hello world!"}}
```

---

### 2. GraphiQL/Playground

Ich vermute, GraphiQL wird [ˈɡɹ̠æ.fɪ.kəl] ausgesprochen (wie „graphical“). Es handelt sich um ein Tool, das unter `localhost:4001` erreichbar ist. Für die anderen Schulungsbeispiele habe ich den __GraphQL Playground__ benutzt (ebenfalls unter der `localhost`-Adresse). Der Unterschied ist nur, dass man sich bei __GraphiQL__ das „query“ vor der Anfrage sparen kann, also ist `{ hello }` völlig ausreichend für die Beispielanfrage. __GraphiQL__ hat eine History-Funktion, __Playground__ benutzt Tabs. So hat man wenigstens beides einmal gesehen.

---

### 3. Postman

Etwas umfangreicher in der Handhabung bei GraphQL. Folgende Felder müssen manuell befüllt werden:

* Methode: `POST`
* URL: `http://localhost:4001`
* Headers: Key: `Content-Type`, Value: `application/json`
* Body (raw): `{ "query": "{ hello }" }`

Die Antwort in der Pretty-Version:

```sh
{
    "data": {
        "hello": "Hello world!"
    }
}
```

---
