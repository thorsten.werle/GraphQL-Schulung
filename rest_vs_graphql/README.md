# REST vs. GraphQL

The purpose of this project is to demonstrate the possibilities of GraphQL in contrast to using a plain REST API. The app contains a to-do list with various functions encapsulating HTTP requests (GET, POST, PUT, DELETE). While the frontend remains the same, it is possible to switch between adapters for a REST and a GraphQL backend.

This repository is part of an exercise in which, given frontend and adapter, the participant is supposed to write one or both versions of the backend themself.
