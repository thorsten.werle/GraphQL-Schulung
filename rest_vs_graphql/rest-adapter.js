let restAdapter = {

    PORT: 5000,

    putTodo: (id, todo) => {
    
        let xhr = new XMLHttpRequest(); 
        let url = `http://localhost:${restAdapter.PORT}/api/todos/${id}`;
        xhr.open('PUT', url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(todo));
    
        xhr.onload = function() {
            if (xhr.status === 201) {
                restAdapter.getTodos();
                const  message = `Entry "${todo.title}" has been updated successfully.`;
                showSuccess(message);
            }
        }
    },
    
    getTodos: () => {
    
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `http://localhost:${restAdapter.PORT}/api/todos`, true);
    
        xhr.onload = function() {
            if (xhr.status == 200) {
                fillTable(xhr.responseText);
            }
        }
    
        xhr.send();
    },
    
    getTodo: id => {
    
        let xhr = new XMLHttpRequest();
        let url = `http://localhost:${restAdapter.PORT}/api/todos/${id}`;
        xhr.open('GET', url, true);
    
        xhr.onload = function() {
            if (xhr.status === 200) {
                fillTable(xhr.responseText);
            } else if (xhr.status === 404) {
                let message = `There is no entry with ID ${id}.`;
                showError(message);
            }
        }
    
        xhr.send();
    },
    
    deleteTodo: id => {

        let xhr = new XMLHttpRequest();
        let url = `http://localhost:${restAdapter.PORT}/api/todos/${id}`;
        xhr.open('DELETE', url, true);
    
        xhr.onload = function() {
            if (xhr.status === 200) {
                restAdapter.getTodos();
            }
        }
    
        xhr.send();
    },
    
    postTodo: todo => {
    
        let xhr = new XMLHttpRequest(); 
        let url = `http://localhost:${restAdapter.PORT}/api/todos/`;
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify(todo));
    
        xhr.onload = function() {
            if (xhr.status === 201) {
                restAdapter.getTodos();
                const  message = `Entry "${todo.title}" has been created successfully.`;
                showSuccess(message);
            }
        }
    },
    
    query: (key, value) => {
        let xhr = new XMLHttpRequest();
        let url = `http://localhost:${restAdapter.PORT}/api/todos?${key}=${value}`;
        xhr.open('GET', url, true);
    
        xhr.onload = function() {
            if (xhr.status === 200) {
                fillTable(xhr.responseText);
            }
        }
    
        xhr.send();
    }
}
