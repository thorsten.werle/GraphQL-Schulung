let graphQlAdapter = {

  PORT: 4000,

  putTodo: (id, todo) => {

    let data = `{
      title: "${todo.title || null}"
      description: "${todo.description || null}"
      priority: ${todo.priority || null}
      difficulty: ${todo.difficulty || null}
      status: ${todo.status || null}
    }`;

    let xhr = new XMLHttpRequest();
    let url = `http://localhost:${graphQlAdapter.PORT}/`;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      query: `mutation {
        updateTodo(id: ${id}, data: ${data}) {
          id
          title
          description
          priority
          difficulty
          status
        }
      }`
    }));

    xhr.onload = function() {
      if (xhr.status === 200) {
        graphQlAdapter.getTodos();
        const message = `Entry "${todo.title}" has been updated successfully.`;
        showSuccess(message);
      }
    }
  },

  getTodos: () => {

    let xhr = new XMLHttpRequest();
    let url = `http://localhost:${graphQlAdapter.PORT}/`;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      query: `query {
        todos {
          id
          title
          description
          priority
          difficulty
          status
        }
      }`
    }));

    xhr.onload = function() {
      if (xhr.status === 200) {
        fillTable(xhr.responseText);
      }
    }
  },

  getTodo: id => {

    let xhr = new XMLHttpRequest();
    let url = `http://localhost:${graphQlAdapter.PORT}/`;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      query: `query {
        todos (id: ${id}) {
          id
          title
          description
          priority
          difficulty
          status
        }
      }`
    }));

    xhr.onload = function() {
      if (xhr.status === 200) {
        fillTable(xhr.responseText);
      }
    }
  },

  deleteTodo: id => {
    let xhr = new XMLHttpRequest();
    let url = `http://localhost:${graphQlAdapter.PORT}/`;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      query: `mutation {
        deleteTodo(id: ${id}) {
          id
          title
          description
          priority
          difficulty
          status
        }
      }`
    }));

    xhr.onload = function() {
      if (xhr.status === 200) {
        graphQlAdapter.getTodos();
      }
    }
  },

  postTodo: todo => {

    let data = `{
      title: "${todo.title}"
      description: "${todo.description}"
      priority: ${todo.priority}
      difficulty: ${todo.difficulty}
      status: ${todo.status}
    }`;

    let xhr = new XMLHttpRequest();
    let url = `http://localhost:${graphQlAdapter.PORT}/`;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      query: `mutation {
        createTodo(data: ${data}) {
          id
          title
          description
          priority
          difficulty
          status
        }
      }`
    }));

    xhr.onload = function() {
      if (xhr.status === 200) {
        graphQlAdapter.getTodos();
        const message = `Entry "${todo.title}" has been created successfully.`;
        showSuccess(message);
      }
    }
  },

  query: (key, value) => {

    let xhr = new XMLHttpRequest();
    let url = `http://localhost:${graphQlAdapter.PORT}/`;
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
      query: `query {
        todos (${key}: "${value}") {
          id
          title
          description
          priority
          difficulty
          status
        }
      }`
    }));

    xhr.onload = function() {
      if (xhr.status === 200) {
        fillTable(xhr.responseText);
      }
    }
  }
}